<?php /* Template Name: Wishlist */

if (!defined('ABSPATH')) exit;

get_header();


?>

    <main id="main" role="main" tabindex="-1">

        <section class="s-wishlist">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="s-wishlist__list">
                            <?php echo do_shortcode('[ti_wishlistsview]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

<?php get_footer(); ?>