<?php /* Template Name: Home */

if (!defined('ABSPATH')) exit;

get_header(); ?>

<main id="main" role="main" tabindex="-1">
    
    <?php get_template_part('template-parts/sections/home/banner'); ?>
    <?php get_template_part('template-parts/sections/home/logos'); ?>
    <?php get_template_part('template-parts/sections/home/sales'); ?>
    <?php get_template_part('template-parts/sections/home/best-sellers'); ?>
    <?php get_template_part('template-parts/sections/home/newest'); ?>
    <?php get_template_part('template-parts/sections/home/newsletter'); ?>
    <?php get_template_part('template-parts/sections/home/instagram'); ?>

</main>

<?php get_footer(); ?>