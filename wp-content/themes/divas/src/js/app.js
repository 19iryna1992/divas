global.$ = global.jQuery = require('jquery');

require('./components/sliders');
require('./components/reset-sales');
require('./components/main-menu');
require('./components/products-filter');