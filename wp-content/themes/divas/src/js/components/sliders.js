import Swiper from 'swiper';
import {Autoplay} from 'swiper/js/swiper.esm';

$(document).ready(function () {

    let homeBannerSlider = $('.JS--home-banner-slider');

    if (homeBannerSlider.length !== 0) {
        var homeBannerSwiper = new Swiper(homeBannerSlider, {
            slidesPerView: 1,
            //loop: true,
               pagination: {
                   el: '.swiper-pagination',
                   type: 'bullets',
                   clickable:true,
               },
        });
    }


    let logosSlider = $('.JS--logos-slider');

    if(logosSlider.length !== 0){
        let logosSliderSwiper = new Swiper(logosSlider, {
            slidesPerView: 6,
            spaceBetween: 15,
            //loop: true,
        })
    }


    // Sales products
    let salesProductsCarousel = $('.JS--sales-products-slider');

    if (salesProductsCarousel.length !== 0) {

        salesProductsCarousel.each(function () {
            var productsSelesSwiper = new Swiper(($(this)[0]), {
                slidesPerView: 3,
                navigation: {
                    nextEl: $(this).closest('.JS--sales-products').find('.JS--related-slider-next'),
                    prevEl: $(this).closest('.JS--sales-products').find('.JS--related-slider-prev'),
                },
                breakpoints: {
                    // when window width is >= 320px
                    320: {
                        slidesPerView: 1.8,
                        spaceBetween: 8,
                        slidesOffsetBefore: 12,
                        slidesOffsetAfter: 12
                    },
                    // when window width is >= 480px
                    576: {
                        slidesPerView: 2,
                        spaceBetween: 14,
                        slidesOffsetBefore: 0,
                        slidesOffsetAfter: 0
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 16,
                        slidesOffsetBefore: 0,
                        slidesOffsetAfter: 0
                    },
                    // when window width is >= 640px
                    1025: {
                        slidesPerView: 3,
                        spaceBetween: 16
                    }
                }
            })
        });

    }

    //Products Sliders Types
    let productsTypesSlider = $('.JS--products-types-slider');

    if (productsTypesSlider.length !== 0) {

        productsTypesSlider.each(function () {
            var productsSwiper = new Swiper(($(this)[0]), {
                slidesPerView: 4,
                //loop: true,
                /*   pagination: {
                       el: '.swiper-pagination',
                       type: 'bullets',
                       clickable:true,
                   },*/
                navigation: {
                    nextEl: $(this).closest('.JS--products-sliders').find('.JS--related-slider-next'),
                    prevEl: $(this).closest('.JS--products-sliders').find('.JS--related-slider-prev'),
                },
                breakpoints: {
                    // when window width is >= 320px
                    320: {
                        slidesPerView: 1.8,
                        spaceBetween: 8,
                        slidesOffsetBefore: 12,
                        slidesOffsetAfter: 12
                    },
                    576: {
                        slidesPerView: 2,
                        spaceBetween: 14,
                        slidesOffsetBefore: 0,
                        slidesOffsetAfter: 0
                    },
                    768: {
                        slidesPerView: 3,
                        spaceBetween: 16,
                        slidesOffsetBefore: 0,
                        slidesOffsetAfter: 0
                    },
                    // when window width is >= 640px
                    1025: {
                        slidesPerView: 4,
                        spaceBetween: 18
                    }
                }
            })
        });

    }


    //Related products slider
        let relatedProductsSlider = $('.JS--related-products-slider');

        if (relatedProductsSlider.length !== 0) {
            var relatedSwiper = new Swiper((relatedProductsSlider), {
                slidesPerView: 4,
                loop: true,
                /*   pagination: {
                       el: '.swiper-pagination',
                       type: 'bullets',
                       clickable:true,
                   },*/
                navigation: {
                    nextEl: '.JS--related-slider-next',
                    prevEl: '.JS--related-slider-prev',
                },
                breakpoints: {
                    // when window width is >= 320px
                    320: {
                        slidesPerView: 1,
                        spaceBetween: 20
                    },
                    // when window width is >= 480px
                    480: {
                        slidesPerView: 3,
                        spaceBetween: 26
                    },
                    // when window width is >= 640px
                    1025: {
                        slidesPerView: 4,
                        spaceBetween: 34
                    }
                }
            })
        }


});