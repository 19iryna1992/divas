$(document).ready(function () {

    const saleTimer = $('#timer');

  /*  console.log(appLocations.salesEnd);
    let endTime = new Date(appLocations.salesEnd).getTime();*/



    if(saleTimer.length !== 0){
        startCounter();
    }

    function startCounter(){

        // End counter time
        var countDownSticker = document.getElementById('sticker-down-time').innerHTML;
        var countDownDate = new Date(countDownSticker).getTime();

// start counter time
        var currentTime = document.getElementById('current-time').innerHTML;
        var currentTimeNumber = new Date(currentTime);
// Increase time on 20 sec for page refresh and show new sticker
        var currentTimeIncrease = new Date(currentTimeNumber.getTime());


        var x = setInterval(function () {

            var now = currentTimeIncrease.setSeconds(currentTimeIncrease.getSeconds() + 1);

            var distance = countDownDate - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);


            document.getElementById('days').innerHTML = (days >= 10) ? days : '0'+days;
            document.getElementById('hours').innerHTML = (hours >= 10) ? hours : '0'+hours;
            document.getElementById('minutes').innerHTML = (minutes >= 10) ? minutes : '0'+minutes;
            document.getElementById('seconds').innerHTML = (seconds >= 10) ? seconds : '0'+seconds;


            if (distance <= 0) {
                document.location.reload(true);
                $('.JS--sales-products').hide();
                resetSale();
                clearInterval(x);

            }
        }, 1000);
    }


    function resetSale() {
        $.ajax({
            url: appLocations.admin_ajax,
            data: {
                action: "reset_sales",
            },
            type: 'POST',
            beforeSend: function () {


            },
            success: function (data) {
                if (!data.success) {
                    return false;
                }

                var result = data.data;
                // Append results to DOM
                if (result.success !== false) {
                    if (result.salesRemoved) {
                        console.log('Hahah');
                        location.reload();
                        //return false;
                    }
                }
            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });

    }

});


