$(document).ready(function () {

  $('.JS-mob-btn').click(function(){
    $(this).toggleClass('active');
    $('.o-header__menu').slideToggle();
  });

  $('.JS--submenu-open').click(function(){
    $(this).next('.c-mega-menu-holder').slideToggle();
    $(this).closest('.has-cats-dropdown').toggleClass('active');
  });

});