$(document).ready(function () {
    let searchParam = window.location.search;

    let filterBtn = $('.JS--sidebar-filter-btn');
    let productSidebar = $('.JS--product-sidebar');

    $('body').on('click','.wpfFilterButton',function(){
         searchParam = window.location.search;
        showHideSubCats(searchParam);
        productSidebar.removeClass('is-active');
    });

    showHideSubCats(searchParam);



    if (productSidebar.length) {
        filterBtn.on('click', function () {
            $(this).toggleClass('is-active');
            productSidebar.toggleClass('is-active');
            $('ul.products').toggleClass('filter-opened');
        });
    }

});

function showHideSubCats(searchParam){
    if(searchParam){
        $('.JS--parent-subcats').fadeOut(300);
    }else{
        $('.JS--parent-subcats').fadeIn(0);
    }
}
