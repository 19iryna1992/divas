<?php

if (!defined('ABSPATH')) exit;

/**
 * Create Global Elements Menu Item
 */
if (function_exists('acf_add_options_page')) {

	# Site Globals (Parent)
	$site_globals = acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title' 	=> 'Options',
		'icon_url'		=> 'dashicons-admin-generic',
		'redirect' 		=> true
	));

	# Headers
	$page_header = acf_add_options_sub_page(array(
		'page_title'  => 'Header Settings',
		'menu_title'  => 'Header Settings',
		'menu_slug'   => 'header',
		'parent_slug' 	=> $site_globals['menu_slug'],
	));

    # Footer
    $page_footer = acf_add_options_sub_page(array(
        'page_title'  => 'Footer Settings',
        'menu_title'  => 'Footer Settings',
        'menu_slug'   => 'footer',
        'parent_slug' 	=> $site_globals['menu_slug'],
    ));

    # Footer
    $page_counter = acf_add_options_sub_page(array(
        'page_title'  => 'Sale counter',
        'menu_title'  => 'Sale Counter',
        'menu_slug'   => 'counter',
        'parent_slug' 	=> $site_globals['menu_slug'],
    ));
    $page_sale = acf_add_options_sub_page(array(
        'page_title'  => 'Sale page',
        'menu_title'  => 'Sale Page',
        'menu_slug'   => 'sale-page',
        'parent_slug' 	=> $site_globals['menu_slug'],
    ));

	# JS Tracking
	$page_tracking_scripts = acf_add_options_page(array(
		'page_title'  => 'Tracking Scripts',
		'menu_title'  => 'Tracking Scripts',
		'menu_slug'   => 'tracking',
		'parent_slug' 	=> $site_globals['menu_slug'],
	));
}
