<?php

if (!defined('ABSPATH')) exit;


/**
 * Extras for ACF
 */
class ACFExtras
{


    function __construct()
    {
        add_action('acf/input/admin_head', array($this, 'collapse_fields'));
    }


    /**
     * Collapse Module Fields on load
     */
    function collapse_fields()
    {
        ?>
        <script type="text/javascript">
            (function(jQuery) {
                jQuery(document).ready(function() {
                    jQuery('.layout').addClass('-collapsed');
                });
            })(jQuery);
        </script>
        <?php
    }
}

new ACFExtras;
