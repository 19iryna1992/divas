<?php

if (!defined('ABSPATH')) exit;

//check if acf is activated
if (!class_exists('ACF')) return;

require_once 'acf-extras.php';
require_once 'acf-sections.php';
require_once 'options-pages.php';
