<?php

if (!defined('ABSPATH')) exit;

/**
 *  divas_line_wrap ()
 *  Gets line breaks from a field and wraps
 *  them in span or list.
 *
 * @param string $type Markup wrapping lines
 * @return  $output
 * @example
 *           divas_line_wrap($fieldname, 'span')
 */
function divas_line_wrap($textarea, $type = "list")
{
    $lines = explode("\n", $textarea);
    $output = '';

    if (!empty($lines)) {
        foreach ($lines as $line) {
            if ($type == 'list') {
                $output .= '<li>' . trim($line) . '</li>';
            } elseif ($type == 'span') {
                $output .= '<span>' . trim($line) . ' ' . '</span>';
            }
        }
    }
    return $output;
}


function load_template_part($template_name, $part_name = null, $args = [])
{
    ob_start();
    get_template_part($template_name, $part_name,$args);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}
