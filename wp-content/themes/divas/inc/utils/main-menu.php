<?php

if (!defined('ABSPATH')) exit;

function product_category_dropdown($args, $field, $post_id)
{
    $args['parent'] = 0;
    return $args;
}

add_filter('acf/fields/taxonomy/query/name=menu_choose_category', 'product_category_dropdown', 10, 3);

/*------------------------------------*\
	Main Menu append mega menu
\*------------------------------------*/
add_filter('walker_nav_menu_start_el', 'append_mega_menu', 15, 4);
function append_mega_menu($item_output, $item, $depth, $args)
{
    // if there's no content, just return the <a> directly
    if ('primary-menu' == $args->theme_location && $depth === 0 && get_field('show_categories_menu', $item)) {
        $parent_category_id = get_field('menu_choose_category', $item);
        $mega_menu ='<span class="menu-item__icon JS--submenu-open"></span>';
        $mega_menu .= '<div class="c-mega-menu-holder">';
        $mega_menu .= load_template_part('template-parts/components/navigation/sub-categories-menu', null, ['parentID' => $parent_category_id]);
        $mega_menu .= '</div>';
        return $item_output . $mega_menu;
    }

    if('primary-menu' == $args->theme_location && $depth === 0 ){
        if (in_array("menu-item-has-children", $item->classes)) {
            $item_output .='<span class="menu-item__icon JS--submenu-open"></span>';
        }
    }


    return $item_output;
}

/*------------------------------------*\
	Menu additional class
\*------------------------------------*/

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects($items, $args)
{
    // loop

    foreach ($items as &$item) {
        // vars
        $show_cat_menu_item = get_field('show_categories_menu', $item);

        if ($show_cat_menu_item) {
            //add class
            array_push($item->classes, 'has-cats-dropdown');
        }
    }
    // return
    return $items;
}
