<?php 

if (!defined('ABSPATH')) exit;

require_once 'a1wm.php';
require_once 'woocommerce/includes.php';
require_once 'contact-form-7.php';
require_once 'yoast.php';