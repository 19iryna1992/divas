<?php
if (!defined('ABSPATH')) exit;

add_filter('woocommerce_billing_fields', 'custom_woocommerce_billing_fields');

function custom_woocommerce_billing_fields($fields)
{

    $fields['billing_nip'] = array(
        'label' => __('NIP', 'woocommerce'), // Add custom field label
        'placeholder' => _x('NIP', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'required' => false, // if field is required or not
        'clear' => false, // add clear or not
        'type' => 'text', // add field type
        'class' => array('my-css'),
        'priority' => 20,// add class name
    );

    return $fields;
}

// save fields to order meta
add_action( 'woocommerce_checkout_update_order_meta', 'divas_save_checkout_new_fields' );

// save new fields values
function divas_save_checkout_new_fields( $order_id ){

    if( !empty( $_POST['billing_nip'] ) ){
        update_post_meta( $order_id, 'billing_nip', $_POST['billing_nip'] );
    }
}
