<?php

if (!defined('ABSPATH')) exit;

add_action('wp_ajax_reset_sales', 'reset_sales');
add_action('wp_ajax_nopriv_reset_sales', 'reset_sales');

function reset_sales()
{

    $end_date = new DateTime(get_field('sale_counter_end', 'option'));
    $now = new DateTime(current_time('Y-m-d H:i:s'));

    $sales_args = array(
        'post_type' => ['product', 'product_variation'],
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );

    $sales_tax = array('meta_query' => array(
        'relation' => 'OR',
        array( // Simple products type
            'key' => '_sale_price',
            'value' => 0,
            'compare' => '>',
            'type' => 'numeric'
        ),
        array( // Variable products type
            'key' => '_min_variation_sale_price',
            'value' => 0,
            'compare' => '>',
            'type' => 'numeric'
        )
    ));
    $sales_products = new WP_Query($args = array_merge($sales_args, $sales_tax));

    if ($sales_products->have_posts() && ($now > $end_date)) :

        while ($sales_products->have_posts()) {
            $sales_products->the_post();
            $id = get_the_ID();
            update_post_meta($id, '_sale_price', '');
            $regular_price = get_post_meta($id, '_regular_price', true);
            update_post_meta($id, '_price', $regular_price);
            /*if (has_term('variable', 'product_type', $id)) {
                variable_product_sync($id);
            }*/
        }
        wp_reset_postdata();

        wp_send_json_success(['salesRemoved' => true]);

    else:
        wp_send_json_error();
    endif;
    die();
}
