<?php


add_action('expire_product_sale', 'expire_product_sale_function');

function expire_product_sale_function()
{

    $end_date = new DateTime(get_field('sale_counter_end', 'option'));
    $now = new DateTime(current_time('Y-m-d H:i:s'));
    $args = array(
        'post_type' => ['product', 'product_variation'],
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_query' => [
            'relation' => 'OR',
            [
                'key' => '_sale_price',
                'value' => 0,
                'compare' => '>',
                'type' => 'numeric'
            ],
            'key' => '_min_variation_sale_price',
            'value' => 0,
            'compare' => '>',
            'type' => 'numeric'
        ]
    );
    $posts = get_posts($args);

    if ($posts && $now > $end_date) {
        foreach ($posts as $p) {
            $id = $p->ID;
            update_post_meta($id, '_sale_price', '');
            $regular_price = get_post_meta($id, '_regular_price', true);
            update_post_meta($id, '_price', $regular_price);
        }
    }
}

