<?php

if (!defined('ABSPATH')) exit;

// Remove sidebar
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

// payment remove
add_filter( 'woocommerce_cart_needs_payment', '__return_false' );

//remove fields from Archive products
//remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_title',5);
//remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

//remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');

//remove single product elements
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);


/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
    function loop_columns()
    {
        return 3; // 3 products per row
    }
}

/**
 * Change number of related products output
 */
function woo_related_products_limit()
{
    global $product;

    $args['posts_per_page'] = 6;
    return $args;
}

add_filter('woocommerce_output_related_products_args', 'divas_related_products_args', 20);
function divas_related_products_args($args)
{
    $args['posts_per_page'] = 8; // 4 related products
    //$args['columns'] = 2; // arranged in 2 columns
    return $args;
}

/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter('loop_shop_per_page', 'new_loop_shop_per_page', 20);

function new_loop_shop_per_page($cols)
{
    // $cols contains the current number of products per page based on the value stored on Options –> Reading
    // Return the number of products you wanna show per page.
    $cols = 9;
    return $cols;
}


add_action('woocommerce_single_product_summary', 'change_single_product_ratings', 2);
function change_single_product_ratings()
{
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
    add_action('woocommerce_single_product_summary', 'wc_single_product_ratings', 9);
}

function wc_single_product_ratings()
{
    global $product;

    $rating_count = $product->get_rating_count();

    if ($rating_count >= 0) {
        $review_count = $product->get_review_count();
        $average = $product->get_average_rating();
        $count_html = '<div class="count-rating">' . array_sum($product->get_rating_counts()) . '</div>';
        ?>


        <div class="woocommerce-product-rating">
            <a href="#reviews" class="woocommerce-review-link" rel="nofollow">
                <span class="woocommerce-product-rating__count">
                    <?php echo round($average, 1); ?> /
                <?php echo $review_count; ?>
                </span>

                <span class="container-rating">
                <?php //echo $review_count; ?>
                <div class="star-rating">
                    <?php echo wc_get_rating_html($average, $rating_count); ?>
                </div>
                <?php //echo $count_html; ?>
              <!--  <?php /*if (comments_open()) : */ ?><a href="#reviews" class="woocommerce-review-link" rel="nofollow">
                    (<?php /*printf(_n('%s customer review', '%s customer reviews', $review_count, 'woocommerce'), '<span class="count">' . esc_html($review_count) . '</span>'); */ ?>
                    )</a>--><?php /*endif */ ?>
            </span>
            </a>
        </div>
        <?php
    }
}


add_filter('woocommerce_pagination_args', 'filter_function_name_3670');
function filter_function_name_3670($array)
{
    $array["prev_text"] = get_double_arrow();
    $array["next_text"] = get_double_arrow();

    return $array;
}

/*-------------Remove my account fields--------------------*/

add_filter('woocommerce_account_menu_items', 'gift_remove_my_account_links');
function gift_remove_my_account_links($menu_links)
{

    unset($menu_links['dashboard']);


    //unset( $menu_links['dashboard'] ); // Remove Dashboard
    //unset( $menu_links['payment-methods'] ); // Remove Payment Methods
    //unset( $menu_links['orders'] ); // Remove Orders
    //unset( $menu_links['downloads'] ); // Disable Downloads
    //unset( $menu_links['edit-account'] ); // Remove Account details tab
    //unset( $menu_links['customer-logout'] ); // Remove Logout link

    return $menu_links;

}


add_filter('woocommerce_sale_flash', 'add_percentage_to_sale_badge', 20, 3);
function add_percentage_to_sale_badge($html, $post, $product)
{

    if ($product->is_type('variable')) {
        $percentages = array();

        // Get all variation prices
        $prices = $product->get_variation_prices();

        // Loop through variation prices
        foreach ($prices['price'] as $key => $price) {
            // Only on sale variations
            if ($prices['regular_price'][$key] !== $price) {
                // Calculate and set in the array the percentage for each variation on sale
                $percentages[] = round(100 - (floatval($prices['sale_price'][$key]) / floatval($prices['regular_price'][$key]) * 100));
            }
        }
        // We keep the highest value
        $percentage = max($percentages) . '%';

    } elseif ($product->is_type('grouped')) {
        $percentages = array();

        // Get all variation prices
        $children_ids = $product->get_children();

        // Loop through variation prices
        foreach ($children_ids as $child_id) {
            $child_product = wc_get_product($child_id);

            $regular_price = (float)$child_product->get_regular_price();
            $sale_price = (float)$child_product->get_sale_price();

            if ($sale_price != 0 || !empty($sale_price)) {
                // Calculate and set in the array the percentage for each child on sale
                $percentages[] = round(100 - ($sale_price / $regular_price * 100));
            }
        }
        // We keep the highest value
        $percentage = max($percentages) . '%';

    } else {
        $regular_price = (float)$product->get_regular_price();
        $sale_price = (float)$product->get_sale_price();

        if ($sale_price != 0 || !empty($sale_price)) {
            $percentage = round(100 - ($sale_price / $regular_price * 100)) . '%';
        } else {
            return $html;
        }
    }
    return '<span class="onsale"><span>' . __('ZNIŻKA', 'divas') . '</span>' . esc_html__('-', 'woocommerce') . ' ' . $percentage . '</span>';
}


//For Archive page add to cart
//add_filter('woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text'); // 2.1 +
function woo_archive_custom_cart_button_text()
{

    if (has_term('preorder', 'product_cat', $product->ID)) :
        return __('Pre order Now!', 'divas');
    else:
        return __('Add to Cart', 'divas');
    endif;
}


add_filter('woocommerce_single_product_carousel_options', 'ud_update_woo_flexslider_options');
function ud_update_woo_flexslider_options($options)
{
    $options['controlNav'] = true;
    //$options['directionNav'] = true;
    $options['slideshow'] = true;
    $options['animationLoop'] = true;
    return $options;
}


/**
 * Change the breadcrumb separator
 */
add_filter('woocommerce_breadcrumb_defaults', 'divas_change_breadcrumb');
function divas_change_breadcrumb($defaults)
{
    $defaults['delimiter'] = '<span class="breadcrumb-delimiter">&nbsp;|&nbsp;</span>';
    $defaults['wrap_before'] = '<div class="woocommerce-breadcrumb-wrapper"><div class="container"><nav class="woocommerce-breadcrumb">';
    $defaults['wrap_after'] = '</nav></div></div>';
    $defaults['home'] = __('Sklep', 'divas');
    return $defaults;
}

/**
 * Replace the home link URL
 */
add_filter('woocommerce_breadcrumb_home_url', 'divas_breadrumb_home_url');
function divas_breadrumb_home_url()
{
    return wc_get_page_permalink('shop');
}

// Remove each style one by one
//add_filter( 'woocommerce_enqueue_styles', 'divas_dequeue_styles' );
function divas_dequeue_styles($enqueue_styles)
{
    //unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
    //unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
    unset($enqueue_styles['woocommerce-smallscreen']);    // Remove the smallscreen optimisation
    return $enqueue_styles;
}


add_filter('woocommerce_loop_add_to_cart_link', 'divas_loo_add_product_link', 10, 3);
function divas_loo_add_product_link($sprintf, $product, $args)
{
    if ($product->is_in_stock()) {
        return '<a href="' . $product->get_permalink() . '" class="button">' . __('POKAŻ WIĘCEJ', 'divas') . '</a>';
    }
    return '';
}

/**
 * Show cart contents / total Ajax
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

function woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;

    ob_start();

    ?>
    <a class="cart-btn c-header-nav__cart-btn" href="<?php echo esc_url(wc_get_cart_url()); ?>">
        <span class="c-header-nav__cart-btn-icon"><?php echo getCartIcon(); ?></span>
        <?php echo $woocommerce->cart->get_cart_total(); ?>
    </a>
    <?php
    $fragments['a.cart-btn'] = ob_get_clean();
    return $fragments;
}



/**
 * Add the code below to your theme's functions.php file
 * to add a confirm password field on the register form under My Accounts.
 */
function woocommerce_registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
    global $woocommerce;
    extract( $_POST );
    if ( strcmp( $password, $password2 ) !== 0 ) {
        return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
    }
    return $reg_errors;
}
add_filter('woocommerce_registration_errors', 'woocommerce_registration_errors_validation', 10, 3);

function woocommerce_register_form_password_repeat() {
    ?>
    <p class="form-row form-row-wide">
        <label for="reg_password2"><?php _e( 'Confirm password', 'woocommerce' ); ?> <span class="required">*</span></label>
        <input type="password" class="input-text" name="password2" id="reg_password2" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" />
    </p>
    <?php
}
add_action( 'woocommerce_register_form', 'woocommerce_register_form_password_repeat' );