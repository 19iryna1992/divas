<?php
function is_product_subcategory() {
    $cat = get_query_var( 'product_cat' );
    $category = get_term_by( 'slug', $cat, 'product_cat' );
    return ( $category->parent !== 0 );
}
