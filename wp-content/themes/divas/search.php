<?php

/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Gift
 */


get_header(); ?>

<main id="main" role="main" tabindex="-1">


<?php get_template_part('template-parts/sections/section-search-result'); ?>


</main>

<?php get_footer(); ?>
