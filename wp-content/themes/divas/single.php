<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package divas
 */

if (!defined('ABSPATH')) exit;

get_header();

while (have_posts()) : the_post();


    ?>

    <main id="main" role="main" tabindex="-1">
        <article>
            <div class="s-single-post">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="s-single-post__image">
                                <?php if (has_post_thumbnail()) : ?>
                                    <?php the_post_thumbnail('full'); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-12 col-md-10 col-lg-9">
                            <div class="s-single-post__content">
                                <h1 class="s-single-post__title"><?php the_title(); ?></h1>
                                <div class="c-wysiwyg">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </main>

<?php endwhile; ?>

<?php get_footer(); ?>