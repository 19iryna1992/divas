<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package divas
 */

if (!defined('ABSPATH')) exit;

get_header();

?>

    <main id="main" role="main" tabindex="-1">

        <?php if (have_posts()) : ?>
            <section class="s-blog">
                <div class="s-blog__intro">
                    <div class="container">
                        <h1 class="s-blog__title"><?php _e('Blog', 'divas'); ?></h1>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <?php while (have_posts()) : the_post(); ?>
                            <div class="col-12 col-sm-6 col-lg-4 py-3">
                                <?php get_template_part('template-parts/components/blog-post'); ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>


                <div class="container">
                    <div class="s-blog__pagination text-center">
                        <?php the_posts_pagination(); ?>
                    </div>
                </div>
            </section>
        <?php endif; ?>
    </main>

<?php get_footer(); ?>