<!-- search -->
<div class="c-search-form JS--search-form-holder">
    <div class="c-search-form__container">
        <form class="c-search-form__form JS--search-form" method="get" action="<?php echo home_url(); ?>" role="search">
            <button class="c-search-form__submit" type="submit" role="button"><?php echo get_search_icon(); ?></button>
            <input class="c-search-form__input JS--search-input" type="search" name="s"
                   value="<?php the_search_query(); ?>"
                   placeholder="<?php _e('Szukaj...', 'divas'); ?>">
            <input type="hidden" name="lang" value="<?php echo apply_filters('wpml_current_language', NULL); ?>"/>
        </form>
    </div>
</div>
<!-- /search -->
