<article class="c-post">
    <div class="c-post__thumbnail">
        <?php if (has_post_thumbnail()) : ?>
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('post-card'); ?>
            </a>
        <?php endif; ?>
    </div>

    <div class="c-post__body">
        <h2 class="c-post__title"><?php the_title(); ?></h2>
        <div class="c-post__excerpt">
            <?php echo get_the_excerpt(); ?>
        </div>
        <a href="<?php the_permalink(); ?>" class="c-post__btn"><?php _e('Read More', 'divas'); ?></a>
    </div>
</article>
