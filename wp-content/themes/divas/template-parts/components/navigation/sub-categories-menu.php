<?php
$args = array(
    'taxonomy' => 'product_cat',
    'parent' => $args['parentID'],
    'orderby' => 'name',
    'order' => 'ASC',
    'hide_empty' => false
);

?>

<div class="container">
    <div class="c-mega-menu">
        <?php foreach (get_categories($args) as $category) :
            $thumbnail_id = get_term_meta( $category->term_id, 'thumbnail_id', true );
            ?>
            <div class="c-mega-menu__item">
                <a href="<?php echo get_category_link($category->term_id); ?>" class="c-category-card">
                    <div class="c-category-card__img d-none d-lg-block">
                        <?php echo wp_get_attachment_image($thumbnail_id); ?>
                    </div>
                    <h4 class="c-category-card__title"><?php echo $category->name; ?></h4>
                </a>

            </div>
        <?php endforeach; ?>
    </div>
</div>
