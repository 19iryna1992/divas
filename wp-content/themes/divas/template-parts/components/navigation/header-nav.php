<?php
$phone = get_field('phone', 'option');
?>
<ul class="c-header-nav">
    <?php if ($phone) : ?>
        <li class="c-header-nav__item">
            <a href="tel:<?php echo str_replace(' ', '', $phone); ?>"
               class="c-header-nav__link"><?php echo $phone; ?></a>
        </li>
    <?php endif; ?>
    <li class="c-header-nav__item">
        <a href="<?php echo wc_customer_edit_account_url(); ?>" class="c-header-nav__link">
            <span class="c-header-nav__icon"><?php echo get_account_user_icon(); ?></span>
        </a>
    </li>
    <li class="c-header-nav__item">
        <a href="/wishlist/" class="c-header-nav__link">
            <span class="c-header-nav__icon"><?php echo get_heart_icon(); ?></span>
        </a>
    </li>
    <li class="c-header-nav__item">
        <a class="cart-btn c-header-nav__cart-btn" href="<?php echo wc_get_cart_url(); ?>">
            <span class="c-header-nav__cart-btn-icon"><?php echo getCartIcon(); ?></span>
            <?php echo WC()->cart->get_cart_total(); ?>
        </a>
    </li>
</ul>

