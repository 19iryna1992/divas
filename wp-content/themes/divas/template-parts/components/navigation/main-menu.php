<?php

if (!defined('ABSPATH')) {
    exit;
}
?>

<?php if (has_nav_menu('primary-menu')): ?>
    <nav class="c-main-menu">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'primary-menu',
            'menu_id' => 'primary-menu',
            'container_class' => 'c-main-menu__container',
            'menu_class' => 'c-main-menu__list',
        ));
        ?>
    </nav>
<?php endif; ?>