<div class="c-product-sidebar">
    <div class="c-product-sidebar__menus">
        <div class="JS--sidebar-filter-btn c-product-sidebar__close-btn d-lg-none">
            <?php echo get_close_icon() ?>
        </div>
        <?php if (is_product_subcategory()): ?>
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('products-sidebar')) : endif; ?>
        <?php else : ?>
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('brands-sidebar')) : endif; ?>
        <?php endif; ?>
    </div>
</div>
