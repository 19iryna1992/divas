<div class="o-footer-logos">
    <?php if (have_rows('payment_logos','option')): ?>
        <div class="o-footer-logos__column">
            <ul class="c-logos c-logos--columns">
                <?php while (have_rows('payment_logos','option')): the_row();
                    $image = get_sub_field('logo');
                    ?>
                    <li class="c-logos__item">
                        <?php echo wp_get_attachment_image($image['id'], 'full'); ?>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if (have_rows('deliveri_logos','option')): ?>
        <div class="o-footer-logos__column">
            <ul class="c-logos">
                <?php while (have_rows('deliveri_logos','option')): the_row();
                    $image = get_sub_field('logo');
                    ?>
                    <li class="c-logos__item">
                        <?php echo wp_get_attachment_image($image['id'], 'full'); ?>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
    <?php endif; ?>
</div>







