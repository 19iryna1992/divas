<?php
if(is_product_category()){
    $term = get_queried_object();
    $children = get_terms('product_cat', array(
        'parent' => $term->term_id,
        'hide_empty' => false
    ));
}


if (!empty($children)) : ?>
    <div class="o-products__categories JS--parent-subcats">
        <?php foreach ($children as $category) :
            $thumbnail_id = get_term_meta($category->term_id, 'thumbnail_id', true);
            ?>
            <div class="c-category-card__wrap">
                <a href="<?php echo get_category_link($category->term_id); ?>" class="c-category-card">
                    <div class="c-category-card__img">
                        <?php echo wp_get_attachment_image($thumbnail_id, 'category-thumb'); ?>
                    </div>
                    <h4 class="c-category-card__title"><?php echo $category->name; ?></h4>
                </a>
            </div>
        <?php endforeach; ?>

    </div>
<?php endif; ?>