<?php
global $woocommerce;
$viewed_products = !empty($_COOKIE['woocommerce_recently_viewed']) ? (array)explode('|', $_COOKIE['woocommerce_recently_viewed']) : array();
$viewed_products = array_filter(array_map('absint', $viewed_products));

$query_args = array(
    'posts_per_page' => 8,
    'post_status'    => 'publish',
    'post_type'      => 'product',
    'post__in'       => $viewed_products,
    'orderby'        => 'rand'
);
// Add meta_query to query args
$query_args['meta_query'] = array();
// Check products stock status
$query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
// Create a new query
$watched_products = new WP_Query($query_args);
if ( $watched_products ) :
?>

<div class="s-recently-viewed-products JS--products-sliders">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="s-sliders__header">
                    <h2 class="c-intro-title"><?php _e('OGLĄDANE WCZEŚNIEJ', 'divas'); ?></h2>
                    <div class="s-sliders__nav">
                        <div class="swiper-button-prev JS--related-slider-prev">
                            <?php echo get_slider_arrow(); ?>
                        </div>
                        <div class="swiper-button-next JS--related-slider-next">
                            <?php echo get_slider_arrow(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="c-slider">
                    <div class="c-slider__wrapper">
                        <div class="c-slider__slider JS--products-types-slider swiper-container">
                            <div class="swiper-wrapper">
                                <?php while ($watched_products->have_posts()) : $watched_products->the_post(); ?>
                                    <div class="swiper-slide">
                                        <div class="c-slider__slide c-product-slide">
                                            <?php wc_get_template_part('content', 'product'); ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>