<?php

if (!defined('ABSPATH')) exit;

$header_message = get_field('top_header_message', 'option');
?>
<?php if (is_front_page() && $header_message) : ?>
    <div class="o-header__top">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="o-header__top-message">
                        <?php echo $header_message; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>