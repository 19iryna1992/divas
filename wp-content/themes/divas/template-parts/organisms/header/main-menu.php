<?php

if (!defined('ABSPATH')) exit;

?>
<div class="o-header__menu">
    <div class="container">
        <div class="row">
            <div class="col-12 position-static">
                <?php get_template_part('template-parts/components/navigation/main-menu'); ?>
            </div>
        </div>
    </div>
</div>