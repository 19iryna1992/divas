<?php

if (!defined('ABSPATH')) exit;


?>
<div class="o-header__main">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-lg-auto order-2 order-lg-1">
                <div class="o-header__main-left justify-content-space-between">
                    <div class="o-header__logo d-none d-lg-block">
                        <?php the_custom_logo(); ?>
                    </div>
                    <div class="o-header__search">
                        <?php get_template_part('template-parts/components/search-form'); ?>
                    </div>
                    <div class="o-header__btn d-lg-none JS-mob-btn">
                        <span></span><span></span><span></span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg order-1 order-lg-2 d-md-flex align-items-center justify-content-between d-lg-block mb-4 mb-lg-3">
                <div class="o-header__logo d-lg-none">
                    <?php the_custom_logo(); ?>
                </div>
                <div class="o-header__nav">
                    <?php get_template_part('template-parts/components/navigation/header-nav'); ?>
                    <div class="c-lang-menu">
                        <?php do_action('wpml_add_language_selector'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>