<?php

if (!defined('ABSPATH')) exit;

?>
<div class="o-footer__main">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="o-footer-navigations">
                    <div class="o-footer-navigations__column">
                        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1')) : endif; ?>
                    </div>
                    <div class="o-footer-navigations__column">
                        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2')) : endif; ?>
                    </div>
                    <div class="o-footer-navigations__column">
                        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-3')) : endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <?php get_template_part('template-parts/components/footer/logos'); ?>
            </div>
        </div>
    </div>
</div>