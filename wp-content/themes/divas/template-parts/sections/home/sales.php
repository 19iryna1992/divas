<?php
$end_date = new DateTime(get_field('sale_counter_end', 'option'));
$now = new DateTime(current_time('Y-m-d H:i:s'));
$today = new DateTime();
$current_date = $today->format('M j, Y H:i:s');


$sales_args = array(
    'post_type' => ['product', 'product_variation'],
    'post_status' => 'publish',
    'posts_per_page' => 12,
);

$sales_tax = array('meta_query' => array(
    'relation' => 'OR',
    array( // Simple products type
        'key' => '_sale_price',
        'value' => 0,
        'compare' => '>',
        'type' => 'numeric'
    ),
    array( // Variable products type
        'key' => '_min_variation_sale_price',
        'value' => 0,
        'compare' => '>',
        'type' => 'numeric'
    )
));
$sales_products = new WP_Query($args = array_merge($sales_args, $sales_tax));

$sale_page_link = get_field('sale_page_link', 'option');

if ($sales_products->have_posts() && ($now < $end_date)) : ?>
    <section class="s-sliders JS--sales-products woocommerce">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="s-sliders__header">
                        <h2 class="c-intro-title"><?php _e('WYPRZEDAŻ', 'divas'); ?></h2>
                        <div class="s-sliders__nav">
                            <div class="swiper-button-prev JS--related-slider-prev">
                                <?php echo get_slider_arrow(); ?>
                            </div>
                            <div class="swiper-button-next JS--related-slider-next">
                                <?php echo get_slider_arrow(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3">
                    <div class="c-timer">
                        <h3 class="c-timer__title"><?php _e('PROMOCJA DO', 'divas'); ?></h3>
                        <div id="timer" class="c-timer__time">
                            <div id="days"></div>
                            <div id="hours"></div>
                            <div id="minutes"></div>
                            <div id="seconds"></div>
                        </div>
                        <div class="c-timer__labels">
                            <div><?php _e('Dni', 'divas'); ?></div>
                            <div><?php _e('God', 'divas'); ?></div>
                            <div><?php _e('Min', 'divas'); ?></div>
                            <div><?php _e('Sek', 'divas'); ?></div>
                        </div>
                        <div class="d-none d-lg-block">
                            <div class="c-timer__footer d-none d-lg-block">
                                <h3 class="c-timer__footer-title"><?php _e('wszystkie promocje', 'divas'); ?></h3>
                                <a href="<?php echo $sale_page_link; ?>" class="c-timer__btn"><?php _e('ZOBAĆ WIĘCEJ', 'divas'); ?></a>
                            </div>
                            <div id="current-time" class="c-counter--hidden" style="display:none;"><?php echo $current_date; ?></div>
                            <div id="sticker-down-time" class="c-counter--hidden" style="display:none;"><?php echo $end_date->format('M j, Y H:i:s'); ?></div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-9">
                    <div class="c-slider">
                        <div class="c-slider__wrapper">
                            <div class="c-slider__slider JS--sales-products-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <?php while ($sales_products->have_posts()) : $sales_products->the_post(); ?>

                                        <div class="swiper-slide">
                                            <div class="c-slider__slide c-product-slide c-product-slide--sales">
                                                <?php wc_get_template_part('content', 'product'); ?>
                                            </div>

                                        </div>
                                    <?php endwhile; ?>
                                    <?php wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 d-lg-none">
                    <div class="c-timer__footer">
                        <h3 class="c-timer__footer-title"><?php _e('wszystkie promocje', 'divas'); ?></h3>
                        <a href="<?php echo $sale_page_link; ?>" class="c-timer__btn"><?php _e('ZOBAĆ WIĘCEJ', 'divas'); ?></a>
                    </div>
                    <div id="current-time" class="c-counter--hidden" style="display:none;"><?php echo $current_date; ?></div>
                    <div id="sticker-down-time" class="c-counter--hidden" style="display:none;"><?php echo $end_date->format('M j, Y H:i:s'); ?></div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>