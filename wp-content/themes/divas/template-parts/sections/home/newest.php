<?php
function filter_where( $where = '' ) {
    // за последние 30 дней
    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-30 days')) . "'";
    return $where;
}

add_filter( 'posts_where', 'filter_where' );
$new_prod_args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => 12,
);

$newest_products = new WP_Query($new_prod_args);
remove_filter( 'posts_where', 'filter_where' );

 if ($newest_products->have_posts()) : ?>
    <section class="s-sliders JS--products-sliders woocommerce">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="s-sliders__header">
                        <h2 class="c-intro-title"><?php _e('Nowości', 'divas'); ?></h2>
                        <div class="s-sliders__nav">
                            <div class="swiper-button-prev JS--related-slider-prev">
                                <?php echo get_slider_arrow(); ?>
                            </div>
                            <div class="swiper-button-next JS--related-slider-next">
                                <?php echo get_slider_arrow(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="c-slider">
                        <div class="c-slider__wrapper">
                            <div class="c-slider__slider JS--products-types-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <?php while ($newest_products->have_posts()) : $newest_products->the_post(); ?>

                                        <div class="swiper-slide">
                                            <div class="c-slider__slide c-product-slide">
                                                <?php wc_get_template_part('content', 'product'); ?>
                                            </div>

                                        </div>
                                    <?php endwhile; ?>
                                    <?php wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>