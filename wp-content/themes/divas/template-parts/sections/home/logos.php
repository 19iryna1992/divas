<?php if (have_rows('logos_slider')): ?>
    <section class="s-logos d-none d-md-block">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="s-logos__wrapper">
                        <div class="s-logos__slider swiper-container JS--logos-slider">
                            <div class="swiper-wrapper">
                                <?php while (have_rows('logos_slider')): the_row();
                                    $logo = get_sub_field('logo');
                                    $link = get_sub_field('logo_link');
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="s-logos__image">
                                            <?php echo ($link) ? '<a href="' . $link . '" target="_blank">' : null; ?>
                                            <?php echo wp_get_attachment_image($logo['ID'], 'logo-thumb'); ?>
                                            <?php echo ($link) ? '</a>' : null; ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>