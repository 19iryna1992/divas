<?php
$title = get_field('newsletter_title');
$description = get_field('newsletter_description');
$form_title = get_field('form_title');
$form = get_field('newsletter_form_shortcode');
?>
<section class="s-newsletter">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 col-xl-7">
                <?php if ($title) : ?>
                    <h2 class="s-newsletter__title u-white"><?php echo $title; ?></h2>
                <?php endif; ?>
                <?php if ($description) : ?>
                    <div class="s-newsletter__content">
                        <?php echo $description; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-6 col-xl-5">
                <div class="s-newsletter__form">
                    <?php if ($form_title) : ?>
                        <h2 class="s-newsletter__title u-white"><?php echo $form_title; ?></h2>
                    <?php endif; ?>
                    <?php if ($form) : ?>
                        <?php echo do_shortcode($form); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
