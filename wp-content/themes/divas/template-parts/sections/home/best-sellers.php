<?php
$sellers_args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => 12,
    'meta_key' => 'total_sales',
    'orderby' => 'meta_value_num'
);

$best_sellers_products = new WP_Query($sellers_args);

if ($best_sellers_products->have_posts()) : ?>
    <section class="s-sliders JS--products-sliders woocommerce">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="s-sliders__header">
                        <h2 class="c-intro-title"><?php _e('BESTSELLERY', 'divas'); ?></h2>
                        <div class="s-sliders__nav">
                            <div class="swiper-button-prev JS--related-slider-prev">
                                <?php echo get_slider_arrow(); ?>
                            </div>
                            <div class="swiper-button-next JS--related-slider-next">
                                <?php echo get_slider_arrow(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="c-slider">
                        <div class="c-slider__wrapper">
                            <div class="c-slider__slider JS--products-types-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <?php while ($best_sellers_products->have_posts()) : $best_sellers_products->the_post(); ?>

                                        <div class="swiper-slide">
                                            <div class="c-slider__slide c-product-slide">
                                                <?php wc_get_template_part('content', 'product'); ?>
                                            </div>

                                        </div>
                                    <?php endwhile; ?>
                                    <?php wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>