<?php
$title = get_field('instagram_title');
$instagram = get_field('instagram_shortcode');
?>

<?php if ($instagram) : ?>
    <section class="s-instagram">
        <div class="container">
            <div class="s-instagram__grid">
                <div class="s-instagram__column">
                    <?php if ($title) : ?>
                        <div class="s-instagram__content">
                            <h2 class="s-instagram__title"><?php echo $title; ?></h2>
                            <div class="s-instagram__icon">
                                <?php echo get_instagram_icon(); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="s-instagram__column--wide">
                    <?php echo do_shortcode($instagram); ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>