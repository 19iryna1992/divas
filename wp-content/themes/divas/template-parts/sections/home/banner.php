<?php if (have_rows('home_banner_slider')): ?>
    <section class="s-banner-slider">
        <div class="swiper-container JS--home-banner-slider">
            <div class="swiper-wrapper">
                <?php while (have_rows('home_banner_slider')): the_row();
                    $image = get_sub_field('image');
                    $button = get_sub_field('button');
                    ?>
                    <div class="swiper-slide u-bg-img" style="background-image: url(<?php echo $image; ?>)">
                        <div class="s-banner-slider__slide">
                            <div class="container">
                                <?php if ($button) : ?>
                                    <a href="<?php echo $button['url']; ?>"
                                       class="s-banner-slider__btn"><?php echo $button['title']; ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </section>
<?php endif; ?>